import React from 'react';
import './header-img.css';

const HeaderImg = () => {
    return (
        <div className="header__imgBox">
            <img className="header__imgBox--img-1" src={require('../../img/planet-background.svg')}  alt='galaxy'/>
            <img className="header__imgBox--img-2 rotate__forward" src={require('../../img/planet-human.svg')}  alt='humans'/>
            <img className="header__imgBox--img-3 rotate__back" src={require('../../img/planets.svg')}  alt='planets'/>
        </div>
    );
};

export default HeaderImg;