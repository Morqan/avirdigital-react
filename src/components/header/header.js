import React from 'react';
import './header.css';
import LinkGroup from '../link-group';
import HeaderList from "../header-list";
import HeaderLogo from "../header-logo";
import HeaderContent from "../header-content";
import HeaderImg from "../header-img";
import HeaderGamburger from "../header-gamburger";

const Header = () => {
  return (
    <header className="header header__bg">
          <HeaderImg/>
          <div className="container">
              <nav className="header__nav">
                <HeaderLogo/>
                <HeaderList/>
                <HeaderGamburger/>

              </nav>
              <div>
                <HeaderContent/>
                <LinkGroup/>
              </div>
          </div>
    </header>
  );
};

export default Header;