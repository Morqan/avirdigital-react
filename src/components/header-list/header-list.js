import React from 'react';
import './header-list.css';

const HeaderList = () => {
    return (

        <ul className="header__list">
            <li className="header__item"><a className="header__item--link" href="#home">Home</a></li>
            <li className="header__item"><a className="header__item--link" href="#about">About</a></li>
            <li className="header__item"><a className="header__item--link" href="#services">Services</a></li>
            <li className="header__item"><a className="header__item--link" href="#portfolio">Portfolio</a></li>
            <li className="header__item"><a className="header__item--link" href="#blog">Blog</a></li>
            <li className="header__item"><a className="header__item--link" href="#contact">Contact</a></li>
        </ul>
    );
};

export default HeaderList;