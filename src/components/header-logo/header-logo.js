import React from 'react';
import './header-logo.css';

const HeaderLogo = () => {
    return (

        <a className="header__logo" href="#home">
            <img className="header__logo--img" src={require('../../img/logo.png')}  alt='logo'/>
        </a>
    );
};

export default HeaderLogo;