import React from 'react';
import './link-group.css';

const LinkGroup = () => {
    return (

            <div className="link__group">
                <a className="link__group--item" href="#service" >Service</a>
                <a className="link__group--item" href="#portfolio" >Portfolio</a>
            </div>
    );
};

export default LinkGroup;