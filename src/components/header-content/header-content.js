import React from 'react';
import './header-content.css';

const HeaderContent = () => {
    return (

        <div className="header__content">
            <h1 className="header__content--title">We Are Creative Agency, Create & Make Your Dream</h1>
            <p className="header__content--text">A team of 100+ experienced developers and designers, ready to help you build your
                web and mobile applications.</p>
        </div>
    );
};

export default HeaderContent;