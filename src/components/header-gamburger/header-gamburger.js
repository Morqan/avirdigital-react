 import React,{Component} from 'react';
 import './header-gamburger.css';


export default class HeaderGamburger extends Component {
    constructor(){
        super();
        this.state = {
            sidenav: true
        }
    }
    changeWidth(){
        this.setState({sidenav: !this.state.sidenav})
    }

    render(){
        let btn_class = this.state.sidenav ? "" : "header__phone--openSidenav";
        return (
            <div className="ml-auto">
                <button className="header__phone--bar"
                        onClick={this.changeWidth.bind(this)}>
                    <i className="fas fa-bars"></i>
                </button>
                <div  className={`header__phone ${btn_class }`}>
                    <a  href="javascript:void(0)" className="header__phone--closeSidenav sidenav__link" onClick={this.changeWidth.bind(this)}>&times;</a>
                    <a className="header__phone--link" href="#home">Home</a>
                    <a className="header__phone--link" href="#about">About</a>
                    <a className="header__phone--link" href="#services">Services</a>
                    <a className="header__phone--link" href="#clients">Clients</a>
                    <a className="header__phone--link" href="#contact">Contact</a>
                </div>
            </div>
        )
    }
}

